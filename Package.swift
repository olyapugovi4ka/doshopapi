// swift-tools-version:4.0
import PackageDescription

let package = Package(
    name: "DOShopApi",
    products: [
        .library(name: "DOShopApi", targets: ["App"]),
    ],
    dependencies: [
        // 💧 A server-side Swift web framework.
        .package(url: "https://github.com/vapor/vapor.git", from: "3.0.0"),
        .package(url: "https://github.com/vapor/fluent-postgresql.git", from: "1.0.0"),
        .package(url: "https://github.com/vapor/postgresql.git", from: "1.4.2"),
        .package(url: "https://github.com/vapor/auth.git", from: "2.0.0")
       //.package(url: "https://github.com/nodes-vapor/paginator.git", from: "4.0.0")

        
    ],
    targets: [
        .target(name: "App", dependencies: ["Authentication","PostgreSQL", "FluentPostgreSQL", "Vapor"]),
        .target(name: "Run", dependencies: ["App"]),
        .testTarget(name: "AppTests", dependencies: ["App"])
    ]
)

