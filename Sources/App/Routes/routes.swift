import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
 let authController = AuthController()
    
    router.group("auth") { (group) in
        group.post("register", use: authController.register)
        group.post("login", use: authController.login)
//        group.post("restore", use: authController.restorePassword)
//        group.post("refresh", use: authController.refreshToken)
    }
    
    let productController = ProductsController()
    router.group("product") { (group) in
        group.get("get_products", use: productController.getProducts)
        group.get("search_products_in_choosed_category", use: productController.searchProductsInChoosedCategory)
        group.get("search_products", use: productController.searchProducts)
    }
    
    let categoryController = CategoryController()
    router.group("category") { group in
         group.post("add_category", use: categoryController.addCategory)
        /*
          var name: String
         */
        group.get("get_categories", use: categoryController.getCategories)
    }
    
    let goodController = GoodController()
    router.group("good") { group in
        group.get("add_good", use: goodController.addGood)
        group.get("get_good", use: goodController.getGood)
    }
    
    let basketController = BasketController()
    router.group("basket") { group in
        group.post("addToBasket", use: basketController.addToBasket)
            group.post("deleteFromBasket", use: basketController.deleteFromBasket)
            group.get("getBasket", use: basketController.getBasket)
    }
    
    
}
