//
//  Users.swift
//  App
//
//  Created by MacBook on 19.04.2020.
//

import FluentPostgreSQL
import Vapor

final class Users: PostgreSQLModel, Hashable {
      typealias Database = PostgreSQLDatabase
    static let entity = "users"
    
    var id:Int? = nil
    var username:String
    var email:String
    var passwordHash:String
    var avatar: String? = nil
    
    static func == (lhs: Users, rhs: Users) -> Bool {
        lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine(id)
    }
    
    init(id: Int? = nil, username: String, email: String, passwordHash:String, avatar:String? = nil){
        self.id = id
        self.username = username
        self.email = email
        self.passwordHash = passwordHash
        self.avatar = avatar
    }
    
   
    
    
    enum CodingKeys: String, CodingKey {
        case id
        case username
        case email
        case passwordHash = "password_hash"
        case avatar
    }
}

extension Users: Parameter {}
extension Users: Content {}
extension Users: Migration {
    static func prepare(on connection: PostgreSQLConnection) -> EventLoopFuture<Void> {
        return PostgreSQLDatabase.create(Users.self, on: connection) { builder in
            builder.field(for: \.id, isIdentifier: true)
            builder.field(for: \.username, type: .varchar, .unique())
            builder.field(for: \.email)
            builder.field(for: \.passwordHash)
            builder.field(for: \.avatar)
        }
    }
}

