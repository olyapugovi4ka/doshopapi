//
//  Reviews.swift
//  App
//
//  Created by MacBook on 27.04.2020.
//

import FluentPostgreSQL
import Vapor

final class Reviews: PostgreSQLModel, Hashable {
      typealias Database = PostgreSQLDatabase
    static let entity = "reviews"
    
    var id:Int? = nil
    var starsCount: Int
    var text: String
    var userID: Users.ID
    var productID: Products.ID
    
    var product: Parent<Reviews, Products> {
        return parent(\.productID)
    }
    
    static func == (lhs: Reviews, rhs: Reviews) -> Bool {
        lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine(id)
    }
    
    init(id: Int? = nil, productID: Products.ID, starsCount: Int , text: String, userID: Users.ID){
        self.id = id
        self.productID = productID
        self.starsCount = starsCount
        self.text = text
        self.userID = userID
        
    }
    
    enum CodingKeys: String, CodingKey {
        case id
            case productID
            case starsCount = "stars_count"
            case text
            case userID
        
    }
}

extension Reviews: Parameter{}
extension Reviews: Content {}
extension Reviews: Migration {
    static func prepare(on connection: PostgreSQLConnection) -> EventLoopFuture<Void> {
        return PostgreSQLDatabase.create(Reviews.self, on: connection) { (builder) in
            builder.field(for: \.id, isIdentifier: true)
            builder.field(for: \.text)
            builder.field(for: \.productID)
            builder.field(for: \.userID)
            builder.field(for: \.starsCount)
            builder.reference(from: \.productID, to: \Products.id)
            builder.reference(from: \.userID, to: \Users.id)
            
        }
    }
}



