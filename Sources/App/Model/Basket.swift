//
//  Basket.swift
//  App
//
//  Created by MacBook on 25.04.2020.
//

import FluentPostgreSQL
import Vapor

final class Basket: PostgreSQLModel, Hashable {
    
    typealias Database = PostgreSQLDatabase
    
    static let entity = "basket"
    
    var id:Int? = nil
    var count: Int
    var productID: Products.ID
    var productName: String
    var totalPrice: Int
    var photo: String
    var category: Parent<Basket, Products> {
        return parent(\.productID)
    }
    
    static func == (lhs: Basket, rhs: Basket) -> Bool {
        lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine(id)
    }
    
    init(id: Int? = nil, productID: Products.ID, productName: String, totalPrice: Int, count: Int, photo: String ){
        self.id = id
        self.productID = productID
        self.productName = productName
        self.totalPrice = totalPrice
        self.count = count
        self.photo = photo
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case productID
        case count
        case productName
        case totalPrice
        case photo
    }
}

extension Basket: Parameter{}
extension Basket: Content {}
extension Basket: Migration {
    static func prepare(on connection: PostgreSQLConnection) -> EventLoopFuture<Void> {
        return PostgreSQLDatabase.create(Basket.self, on: connection) { (builder) in
            builder.field(for: \.id, isIdentifier: true)
            builder.field(for: \.productID)
            builder.field(for: \.productName)
            builder.field(for: \.count)
            builder.field(for: \.totalPrice)
            builder.field(for: \.photo)
            builder.reference(from: \.productID, to: \Products.id)
        }
    }
}


