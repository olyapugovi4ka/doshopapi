//
//  Categories.swift
//  App
//
//  Created by MacBook on 22.04.2020.
//

import FluentPostgreSQL
import Vapor

final class Categories: PostgreSQLModel, Hashable {
     typealias Database = PostgreSQLDatabase
    static let entity = "categories"
    
    var id:Int? = nil
    var name:String
    
    
    static func == (lhs: Categories, rhs: Categories) -> Bool {
        lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine(id)
    }
    
    init(id: Int? = nil, name: String){
        self.id = id
        self.name = name
       
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        
    }
}

extension Categories: Parameter {}
extension Categories: Content {}
extension Categories: Migration {
    
    static func prepare(on connection: PostgreSQLConnection) -> EventLoopFuture<Void> {
        return PostgreSQLDatabase.create(Categories.self, on: connection) { builder in
            builder.field(for: \.id, isIdentifier: true)
            builder.field(for: \.name, type: .varchar, .unique())

        }
    }
}
