//
//  Products.swift
//  App
//
//  Created by MacBook on 22.04.2020.
//

import FluentPostgreSQL
import Vapor

final class Products: PostgreSQLModel, Hashable {
      typealias Database = PostgreSQLDatabase
    static let entity = "products"
    
    var id:Int? = nil
    var productName:String
    var price:Int
    var description:String
    var photo: String
    var count: Int
    
    static func == (lhs: Products, rhs: Products) -> Bool {
        lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine(id)
    }
    
    var categoryID: Categories.ID
    
    var category: Parent<Products, Categories> {
        return parent(\.categoryID)
    }
    
    init(id: Int? = nil, productName: String, price: Int, description:String, photo:String,count: Int , categoryID:Categories.ID){
        self.id = id
        self.productName = productName
        self.price = price
        self.description = description
        self.count = count
        self.photo = photo
        self.categoryID = categoryID
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case productName = "product_name"
        case price
        case description
        case count
        case photo
        case categoryID
    }
}
//extension Products: Paginatable {}
extension Products: Parameter{}
extension Products: Content {}
extension Products: Migration {
    static func prepare(on connection: PostgreSQLConnection) -> EventLoopFuture<Void> {
        return PostgreSQLDatabase.create(Products.self, on: connection) { (builder) in
            builder.field(for: \.id, isIdentifier: true)
            builder.field(for: \.productName)
            builder.field(for: \.price)
            builder.field(for: \.description)
            builder.field(for: \.photo)
            builder.field(for: \.count)
            builder.field(for: \.categoryID)
            builder.reference(from: \.categoryID, to: \Categories.id)
        }
    }
}

