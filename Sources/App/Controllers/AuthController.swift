//
//  AuthController.swift
//  App
//
//  Created by MacBook on 19.04.2020.
//

import Vapor
import Crypto

struct RegisterUserRequest: Codable {
    var username: String
    var password: String
    var email: String
}

struct RegisterUserResponse: Content {
     var accessToken: String
       var refreshToken: String
       var expiresAt: Int? = nil
}

struct AuthRequest: Codable {
    var username: String
    var password: String
}

struct AuthResponse: Content {
    var userId: Int
    var accessToken: String
    var refreshToken: String
    var expiresAt: Int? = nil
}

final class AuthController {
    
    
    func register(_ req: Request) throws -> Future<RegisterUserResponse> {
        return try req.content.decode(RegisterUserRequest.self).flatMap { user in
            
            if user.password.count < 6 {
                throw Abort(.badRequest, reason: "Not correct password")
            }
            let restorePassword = try BCrypt.hash(user.password)
            let newUser = Users(username: user.username, email: user.email, passwordHash: restorePassword)

            return newUser.save(on: req).flatMap { createdUser in
                let accessToken = try UserToken.create(userID: createdUser.requireID())
                return accessToken.save(on: req).map { resultToken in
                RegisterUserResponse(accessToken: resultToken.accessToken, refreshToken: resultToken.refreshToken)
                }
            }
        }
    }
    
    func login(_ req: Request) throws -> Future<AuthResponse> {
        return try req.content.decode(AuthRequest.self).flatMap { user in
            Users.query(on: req).filter(\Users.username,.equal, user.username).first().flatMap { result in
                guard let result = result,
                    let userID = result.id else {
                    throw Abort(.badRequest, reason:"User does not exist")
                }
                
                guard try BCrypt.verify(user.password, created: result.passwordHash) else {
                    throw Abort(.badRequest, reason: "You entered incorrect data")
                }
                
                return UserToken.query(on: req).filter(\UserToken.userID, .equal, userID).first().map { userToken in
                    guard let token = userToken?.accessToken,
                        let expiresAt = userToken?.expiresAt,
                        let refreshToken = userToken?.refreshToken else {
                            throw Abort(.badRequest, reason: "You entered incorrect data")
                    }
                    return AuthResponse(userId: userID, accessToken: token, refreshToken: refreshToken, expiresAt: expiresAt)
                }
                
                
                
            }
        }
    }
    
//    func restorePassword(_ req: Request) -> Future<HTTPResponse> {
//        
//    }
//    
//    func refreshToken(_ req: Request) -> Future<HTTPResponse> {
//        
//    }
}
