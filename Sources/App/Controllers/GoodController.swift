//
//  GoodController.swift
//  App
//
//  Created by ios_Dev on 15.05.2020.
//

import Vapor

// addGood
struct AddGoodRequest:Codable {
    var productName: String
    var price: Int
    var description: String
    var photo: String
    var count:Int
    var categoryID: Categories.ID
}

struct AddGoodResponse: Content {
    var result: Int
    var message: String
}

// getGood
struct GetGoodRequest: Codable {
    var id: Int
}

struct GetGoodResponse: Content {
    var result: Int
    var name: String
    var price: Int
    var decription: String
}

final class GoodController {
    
    func addGood(_ req: Request) throws -> Future<AddGoodResponse> {
        return try req.content.decode(AddGoodRequest.self).flatMap { good in
            let newGood = Products(productName: good.productName, price: good.price, description: good.description, photo: good.photo, count: good.count, categoryID: good.categoryID)
            return newGood.save(on: req).map { createdGood in
                let message = "\(createdGood.productName) succesfully saved"
                return AddGoodResponse(result: 1, message: message)
                
            }
        }
    }
    
    func getGood(_ req: Request) throws -> Future<GetGoodResponse> {
           return try req.content.decode(GetGoodRequest.self).flatMap({ (good) in
               return Products.query(on: req).filter(\Products.id, .equal, good.id).first().map { product in
                   guard let product = product  else {
                       throw Abort(.badRequest, reason:"Product does not exist")
                   }
                   return GetGoodResponse(result: 1, name: product.productName, price: product.price, decription: product.description)
               }
           })
           
       }
    
}
