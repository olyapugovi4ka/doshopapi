//
//  CategoryController.swift
//  App
//
//  Created by ios_Dev on 15.05.2020.
//

import Vapor

struct AddCategoryRequest: Codable {
    var name: String
}

struct AddCategoryReturnResponse: Content {
    var result: Int
    var message: String
}

struct GetCategoriesResponse: Content {
    var categories: [Categories]
}

final class CategoryController {
    
    func addCategory(_ req: Request) throws -> Future<AddCategoryReturnResponse>{
           return try req.content.decode(AddCategoryRequest.self).flatMap { category in
               let category = Categories(name: category.name)
               return category.save(on: req).map { category in
                   let message = "\(category.name) succesfully saved"
                   return AddCategoryReturnResponse(result: 1, message: message)
               }
           }
       }
    
    func getCategories(_ req: Request) throws -> Future<GetCategoriesResponse> {
        return Categories.query(on: req).all().map { categories in
            return GetCategoriesResponse(categories: categories)
        }
    }
}
