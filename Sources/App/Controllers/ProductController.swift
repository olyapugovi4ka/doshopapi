//
//  ProductController.swift
//  App
//
//  Created by MacBook on 22.04.2020.
//

import Vapor


// getProducts
struct GetProductsRequest: Codable {
    var offset: Int
    var limit: Int
    var idCategory: Int
}

struct GetProductsResponse: Content {
    var count: Int
    var productes:[NecessaryInfoAboutProduct]
}

struct NecessaryInfoAboutProduct: Content {
    var id: Int
    var name: String
    var price: Int
}

// searchProducts
struct SearchProductsInChoosedCategoryRequest: Codable {
    var idCategory: Int
    var query: String
}
struct SearchProductsRequest: Codable {
    var query: String
}
// searchProductResponse is equal to getProductsResponse

final class ProductsController {
    
    func getProducts(_ req: Request) throws -> Future<GetProductsResponse>{
        return try req.content.decode(GetProductsRequest.self).flatMap { request in
            let offset = request.offset
            let limit = request.limit
            let idCategory = request.idCategory
            
            return Products.query(on: req)
                .filter(\Products.categoryID, .equal, idCategory)
                .range(lower: offset * limit, upper: offset * limit + limit)
                .all().map { (results) -> GetProductsResponse in
                    var commonCatalogArray:[NecessaryInfoAboutProduct] = []
                    for result in results {
                        guard let id = result.id  else {
                            throw Abort(.badRequest, reason:"Category does not exist")
                        }
                        let commomCatalogeResponse = NecessaryInfoAboutProduct(id: id, name: result.productName, price: result.price)
                        commonCatalogArray.append(commomCatalogeResponse)
                    }
                    return GetProductsResponse(count: commonCatalogArray.count, productes: commonCatalogArray)
            }
        }
    }
    
    // search productes in choosed category
    func searchProductsInChoosedCategory(_ req: Request) throws -> Future<GetProductsResponse> {
        return try req.content.decode(SearchProductsInChoosedCategoryRequest.self).flatMap { request in
            let idCategory = request.idCategory
            return Products.query(on: req)
                .filter(\Products.categoryID, .equal, idCategory)
                .filter(\Products.productName, "~", request.query)
                .range(..<20)
                .all().map { results -> GetProductsResponse in
                    var commonCatalogArray:[NecessaryInfoAboutProduct] = []
                    for result in results {
                        guard let id = result.id  else {
                            throw Abort(.badRequest, reason:"Searched product does not exist")
                        }
                        let commomCatalogeResponse = NecessaryInfoAboutProduct(id: id, name: result.productName, price: result.price)
                        commonCatalogArray.append(commomCatalogeResponse)
                    }
                    return GetProductsResponse(count: commonCatalogArray.count, productes: commonCatalogArray)
            }
        }
    }
    
    // search productes by the common list of productes
    func searchProducts(_ req: Request) throws -> Future<GetProductsResponse> {
        return try req.content.decode(SearchProductsRequest.self).flatMap { request in
            return Products.query(on: req)
                .filter(\Products.productName, "~", request.query)
                .range(..<20)
                .all().map { results -> GetProductsResponse in
                    var commonCatalogArray:[NecessaryInfoAboutProduct] = []
                    for result in results {
                        guard let id = result.id  else {
                            throw Abort(.badRequest, reason:"Searched product does not exist")
                        }
                        let commomCatalogeResponse = NecessaryInfoAboutProduct(id: id, name: result.productName, price: result.price)
                        commonCatalogArray.append(commomCatalogeResponse)
                    }
                    return GetProductsResponse(count: commonCatalogArray.count, productes: commonCatalogArray)
            }
        }
    }
    
    /*   func loadAvatar(_ req: Request) throws -> Future<AvatarResponse> {
        return try req.content.decode(UploadAttachment.self).flatMap(to: AvatarResponse.self) { user in
            return UserToken.query(on: req).filter(\UserToken.access_token, .equal, user.token)
                .first().flatMap { userToken in

                guard let token = userToken else {
                    throw Abort(.badRequest, reason: "You entered incorrect data")
                }

                let directory = DirectoryConfig.detect()
                let workPath = directory.workDir

                let name = UUID().uuidString + ".png"
                let imageFolder = "Public/images"
                let saveURL = URL(fileURLWithPath: workPath).appendingPathComponent(imageFolder, isDirectory: true).appendingPathComponent(name, isDirectory: false)
                try user.file.write(to: saveURL)

                return token.user.get(on: req).flatMap { user in
                    user.avatar = name
                    return user.update(on: req).map { _ in
                        return AvatarResponse(path: "images/" + name)
                    }
                }
            }
        }
    }*/
}
