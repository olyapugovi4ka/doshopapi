//
//  BasketController.swift
//  App
//
//  Created by MacBook on 25.04.2020.
//

import Vapor

// addToBasket
struct AddToBasketRequest: Codable {
    var productId: Int
    var count:Int
}

struct AddToBaskerResponse: Content {
    var result: Int
    var message: String
}

// deleteFromBasket
struct DeleteFromBasketRequest: Codable {
    var productId: Int
}

struct DeleteFromBaskerResponse: Content {
    var result: Int
    var message: String
}

// getBasket
struct GetBasketRequest: Codable {
    var userID:Int
}

struct GetBasketResponse: Content {
    var totalSum: Int
    var countGoods: Int
    var contents:[Basket]
}

final class BasketController {
    
    func addToBasket(_ req: Request) throws -> Future<AddToBaskerResponse>{
        return try req.content.decode(AddToBasketRequest.self).flatMap({ request in
            Products.query(on: req).filter(\Products.id, .equal, request.productId).first().flatMap { good in
                guard let good = good,
                    request.count <= good.count else {
                        throw Abort(.badRequest, reason:"Not enouch count")
                }
                
                let totalPrice = request.count * good.price
                let basketPosition = Basket(productID: request.productId, productName: good.productName, totalPrice: totalPrice, count: request.count, photo: good.photo)
                return basketPosition.save(on: req).map { basketPosition in
                    let message = "\(basketPosition.productID) succesfully saved"
                    return AddToBaskerResponse(result: 1, message: message)
                }
            }
            
        })
    }
    func deleteFromBasket(_ req: Request) throws -> Future<DeleteFromBaskerResponse>{
        return try req.content.decode(DeleteFromBasketRequest.self).flatMap{ request in
             Basket.query(on: req)
                .filter(\Basket.productID, .equal, request.productId)
                .first().flatMap { deletedProduct in
                    guard let deletedProduct = deletedProduct else {
                            throw Abort(.badRequest, reason:"Not enouch count")
                    }
                    return deletedProduct.delete(on: req).map { _ in
                        let message = "\(deletedProduct.productName) succesfully deleted"
                        return DeleteFromBaskerResponse(result: 1, message: message)
                        
                    }
            }
        }
    }
    
    func getBasket(_ req: Request) throws -> Future<GetBasketResponse>{
        return try req.content.decode(GetBasketRequest.self).flatMap { request in
            return Basket.query(on: req).filter(\Basket.id, .equal, request.userID).all().map { basket  in
                let amount = basket.map { $0.totalPrice }
                    .reduce(0, +)
                return GetBasketResponse(totalSum: amount,
                                         countGoods: basket.count,
                                         contents: basket)
            }
        }
    }

}
