import FluentPostgreSQL
import Authentication
import PostgreSQL
import Vapor

/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    
    let router = EngineRouter.default()//создаем дефолтный роутер
    try routes(router)// регистрируем роутер
    
    
    try services.register(FluentPostgreSQLProvider())//регистрируем провайдер
    try services.register(AuthenticationProvider())//регистрируем провайдер
    services.register(router, as: Router.self)//закрепляем в сервисе
    
    //конфигурируем промежуточный слой
    var middleware = MiddlewareConfig()
    middleware.use(FileMiddleware.self)
    middleware.use(ErrorMiddleware.self)
    services.register(middleware)
    
    //работа с базой данных
    let pgConfig = PostgreSQLDatabaseConfig(hostname: "127.0.0.1",
                                            port: 5432,
                                            username: "postgres",
                                            database: "DOShop",
                                            password: Secret.password,
                                            transport: .cleartext)
    //конннекшн
    let pgConnection = PostgreSQLDatabase(config: pgConfig)
    var database = DatabasesConfig()
    database.add(database: pgConnection, as: .psql)
    database.enableLogging(on: .psql)
    services.register(database)
    
    Users.defaultDatabase = .psql
    UserToken.defaultDatabase = .psql
    Categories.defaultDatabase = .psql
    Products.defaultDatabase = .psql
    Basket.defaultDatabase = .psql
    Reviews.defaultDatabase = .psql
    
    var migrations = MigrationConfig()
    migrations.add(model: Users.self, database: .psql)
    migrations.add(model: UserToken.self, database: .psql)
    migrations.add(model: Categories.self, database:.psql)
    migrations.add(model: Products.self, database:.psql)
    migrations.add(model: Basket.self, database: .psql)
    migrations.add(model: Reviews.self, database: .psql)
    services.register(migrations)
    
    //Prepare websoket to listen users calls
    let socket = SocketServer()
    socket.start(&services)
    
}
