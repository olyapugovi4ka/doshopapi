//
//  MessageController.swift
//  App
//
//  Created by ios_Dev on 26.05.2020.
//

import Vapor

class MessageController {
    
    func addMessage(req: Request, data: WebSocketRequestData, session: SocketSession) {
        guard let message = data.data?["message"] as? String else {
            let result = SocketResponse<String>(id: data.id,
                                              event: nil,
                                              error: "Incorrect parameters",
                                              result: nil)
            session.socket.send(getResponse(response: result))
            return
        }
        
        let response = SocketResponse<String>(id: data.id,
                                              event: nil,
                                              error: nil,
                                              result: message)
        SocketServer.connections.forEach { user in
            user.socket.send(getResponse(response: response))
        }
    }
}
