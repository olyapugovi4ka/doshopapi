import Vapor

class SocketServer {

    /// Connected users.
    static var connections = [SocketSession]()

    /// Router to navigate user requests.
    let router = WebSocketRouter()

    /// Get request from user to connection.
    func connectUser(ws: WebSocket, req: Request) {

            // Add user session to connection list.
            let session = SocketSession(id: UUID().uuidString, socket: ws)
            SocketServer.connections.append(session)

            // Send info that connection was successfull.
            let encoder = JSONEncoder()
            if let response = try? encoder.encode(["status":"true"]),
               let message = String(bytes: response, encoding: .utf8){
                ws.send(message)
            }

            // Start listen user events.
            ws.onText { [weak self] ws, text in
                self?.router.handle(req: req,
                                   text: text, session: session)
            }

            // Catch the disconnect from the user
            ws.onCloseCode { code in
                SocketServer.connections.removeAll { $0.id == session.id }
            }

            try? req.releaseCachedConnections()
        }
    

    /// Start socket listen events.
    func start(_ services: inout Services) {
        let wss = NIOWebSocketServer.default()

        wss.get("/") { ws, req in
            self.connectUser(ws: ws, req: req)
        }

        services.register(wss, as: WebSocketServer.self)
    }
}
