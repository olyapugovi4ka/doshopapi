//
//  WebSocketRouter.swift
//  App
//
//  Created by ios_Dev on 26.05.2020.
//

import Vapor

class WebSocketRouter {
    func handle(req: Request, text: String, session: SocketSession) {
        configureChat(req: req, text: text, session: session)
    }

    func handleMethod(method: String, path: String, action: (_ params: WebSocketRequestData) -> ()) {
        if path.starts(with: method) {
            let data = String(path.dropFirst(method.count))

            guard let json = data.json(),
                let id = json["id"] as? String else {
                    return
            }
            let params = json["data"] as? [String: Any]

            action(WebSocketRequestData(id: id, data: params))
        }
    }
}
extension WebSocketRouter {
    func configureChat(req: Request, text: String, session: SocketSession) {
        let conversationController = MessageController()
        
        handleMethod(method: "chat/addMessage/", path: text) { request in
            conversationController.addMessage(req: req, data: request, session: session)
        }
    }
}
