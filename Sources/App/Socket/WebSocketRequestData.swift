//
//  WebSocketRequestData.swift
//  App
//
//  Created by ios_Dev on 26.05.2020.
//

struct WebSocketRequestData {
    var id: String
    var data: [String: Any]?
    
    subscript(key: String) -> Any? {
        guard let data = data else {
            return nil
        }
        return data[key]
    }
}
