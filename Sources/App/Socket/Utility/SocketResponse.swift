//
// SockerResponse.swift
//  App
//
//  Created by ios_Dev on 26.05.2020.
//

struct SocketResponse<T: Encodable>: Encodable {
    var id: String
    var event: String?
    var error: String?
    var result: T?
}
