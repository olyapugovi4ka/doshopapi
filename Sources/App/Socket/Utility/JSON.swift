//
//  JSON.swift
//  App
//
//  Created by ios_Dev on 26.05.2020.
//

import Foundation

extension String {
    /// Convert json string to dictionary format.
    func json() -> [String: Any]? {
        guard let data = self.data(using: .utf8) else {
            return nil
        }

        let json = try? JSONSerialization.jsonObject(with: data, options: [])

        if let object = json as? [String: Any] {
            return object
        }
        return nil
    }
}

func getResponse<T: Encodable>(response: SocketResponse<T>) -> String {
    let encoder = JSONEncoder()

    if let response = try? encoder.encode(response),
       let message = String(bytes: response, encoding: .utf8) {
        return message
    }

    return "Bad response"
}

