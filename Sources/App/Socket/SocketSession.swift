//
//  SocketSession.swift
//  App
//
//  Created by ios_Dev on 26.05.2020.
//

import Vapor

final class SocketSession: Hashable {
    var id: String
    var socket: WebSocket

    init(id: String, socket: WebSocket) {
        self.id = id
        self.socket = socket
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    static func == (lhs: SocketSession, rhs: SocketSession) -> Bool {
        return lhs.id == rhs.id
    }
}
